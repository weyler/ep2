O program possui uma interface como meio de comunicação com o usuário.

Tal interface requer a escolha da imagem a ser usada, o filtro a se aplicar e caso seja escolhido o filtro da mensagem escondida, o programa requer a posição inicial da mensagem.

As imagens filtradas e/ou mensagem escondida aparecem em uma nova janela.

A cada aplicação de filtro o programa deve ser reiniciado.

As imagens a serem dadas como entrada no programa devem estar na pasta raiz do projeto.
