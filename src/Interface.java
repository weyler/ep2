import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Interface {

   private JFrame mainFrame;
   private JLabel headerLabel;
   private JLabel statusLabel;
   private JPanel controlPanel;
   private JTextField nome;
   private JTextField textposicao;
   private String text = "";
   private int escolha_do_filtro = 0;
   private String posicao;
   private int posicaoint = 0;

   public Interface(){
      preparaGUI();
   }
      
   private void preparaGUI(){
      mainFrame = new JFrame("Aplicação de filtros em imagens (.pgm/.ppm)");
      mainFrame.setSize(500,500);
      mainFrame.setLayout(new GridLayout(4, 3));

      headerLabel = new JLabel("",JLabel.CENTER );
      statusLabel = new JLabel("",JLabel.CENTER);        

      mainFrame.addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent windowEvent){
	        System.exit(0);
         }        
      });    
      controlPanel = new JPanel();
      controlPanel.setLayout(new FlowLayout());

      mainFrame.add(headerLabel);
      mainFrame.add(controlPanel);
      mainFrame.add(statusLabel);
      mainFrame.setVisible(true);
   }

   void mostrarExemplo(){
      headerLabel.setText("A imagem deve estar na pasta raiz do projeto");
      
      //nome = new JTextField(20);
      textposicao = new JTextField(20);
      while (text.equals("")){
      JFileChooser chooser = new JFileChooser();
      FileNameExtensionFilter filter = new FileNameExtensionFilter("Imagens PGM e PPM", "pgm", "ppm");
      chooser.setFileFilter(filter);
      Component parent = null;
      int returnVal = chooser.showOpenDialog(parent);
      if(returnVal == JFileChooser.APPROVE_OPTION) {
              text = chooser.getSelectedFile().getName();
      }
      }
      
      String ext = text.substring(text.lastIndexOf('.') + 1);
      
      //nome.addActionListener(new TextListener());
      textposicao.addActionListener(new PosicaoListener());

      JButton Button0 = new JButton("Filtro B");
      JButton Button1 = new JButton("Filtro G");
      JButton Button2 = new JButton("Filtro R");
      JButton Button3 = new JButton("Filtro negativo");
      JButton Button4 = new JButton("Filtro Bytes");
      JButton Button5 = new JButton("Filtro negativo");
      JButton Button6 = new JButton("Filtro Blur");
      JButton Button7 = new JButton("Filtro Sharpen");
      

      Button0.setActionCommand("-8");
      Button1.setActionCommand("1");
      Button2.setActionCommand("2");
      Button3.setActionCommand("3");
      Button4.setActionCommand("4");
      Button5.setActionCommand("5");
      Button6.setActionCommand("6");
      Button7.setActionCommand("7");

      Button0.addActionListener(new ButtonClickListener());
      Button1.addActionListener(new ButtonClickListener()); 
      Button2.addActionListener(new ButtonClickListener()); 
      Button3.addActionListener(new ButtonClickListener()); 
      Button4.addActionListener(new ButtonClickListener()); 
      Button5.addActionListener(new ButtonClickListener()); 
      Button6.addActionListener(new ButtonClickListener()); 
      Button7.addActionListener(new ButtonClickListener()); 
      
      
      if (ext.equals("ppm")){
      controlPanel.add(Button0);
      controlPanel.add(Button1);
      controlPanel.add(Button2);
      controlPanel.add(Button3);
      }
      if (ext.equals("pgm")){
      controlPanel.add(Button4);
      controlPanel.add(Button5);
      controlPanel.add(Button6);
      controlPanel.add(Button7);
      }
      
      mainFrame.setVisible(true);
      
   }
   
   private class TextListener implements ActionListener{
	   public void actionPerformed(ActionEvent a){
		   text = nome.getText();
	   }
   }
   private class PosicaoListener implements ActionListener{
	   public void actionPerformed(ActionEvent b){
		   posicao = textposicao.getText();
		   posicaoint = Integer.parseInt(posicao);
	   }
   }
   private class ButtonClickListener implements ActionListener{
      public void actionPerformed(ActionEvent e) {
         String command = e.getActionCommand();  
         if( command.equals( "-8" ))  {
        	 escolha_do_filtro = -8;
         }
         if( command.equals( "1" ))  {
             escolha_do_filtro = 1;
          }
         if( command.equals( "2" ))  {
             escolha_do_filtro = 2;
          }
         if( command.equals( "3" ))  {
             escolha_do_filtro = 3;
          }
         if( command.equals( "4" ))  {
             escolha_do_filtro = 4;
             JFrame frame2 = new JFrame("Posição incial da mensagem");
             frame2.setSize(500,500);
             frame2.setLayout(new GridLayout(4, 3));

             JLabel header = new JLabel("",JLabel.CENTER );       

             frame2.addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent windowEvent){
       	        System.exit(0);
                }        
             });    
             JPanel control = new JPanel();
             control.setLayout(new FlowLayout());

             frame2.add(header);
             frame2.add(control);
             frame2.setVisible(true);
             
             header.setText("Insira a posição inicial da mensagem");
             
             textposicao = new JTextField(20);
             textposicao.addActionListener(new PosicaoListener());
             control.add(textposicao);
          }
         if( command.equals( "5" ))  {
             escolha_do_filtro = 5;
          }
         if( command.equals( "6" ))  {
             escolha_do_filtro = 6;
          }
         if( command.equals( "7" ))  {
             escolha_do_filtro = 7;
          }
      }		
   }
   
   public String getText(){
	return text;
   }
   
   public int getPosicao(){
	   return posicaoint;
   }
   
   public int getFiltro(){
	   return escolha_do_filtro;
   }
}

