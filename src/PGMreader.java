import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.DataBuffer;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.*;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class PGMreader {

	public static String le_linha(FileInputStream arquivo) {
		String linha = "";
		byte bb;
		try {
			while ((bb = (byte) arquivo.read()) != '\n') {
				linha += (char) bb;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		//System.out.println("Linha: " + linha);
		return linha;
	}
	public static void main(String args[]) throws Exception {
		int posicao = 0;
		String nome_arquivo = null;
		int escolha_do_filtro = 0;
		Interface exemplo = new Interface();
		exemplo.mostrarExemplo();
		while(escolha_do_filtro == 0){
			escolha_do_filtro = exemplo.getFiltro();
			if(escolha_do_filtro == 4){
				while(posicao == 0){
					posicao = exemplo.getPosicao();
					Thread.sleep(100);
				}
			}
			Thread.sleep(100);
		}
		nome_arquivo = exemplo.getText();
		try {
			//System.out.println(nome_arquivo);
			FileInputStream arquivo = new FileInputStream(nome_arquivo);
			BufferedImage imagem_pgm = null; //imagem_pgm_negativo = null;
			String numeroMagico = null;
			//String posicaoString = null;
		    int width = 0;
		    int height = 0;
		    int maxVal = 0;
			byte bb;
			
			numeroMagico = PGMreader.le_linha(arquivo);
			String linha = null;
			if("P5".equals(numeroMagico) || "P6".equals(numeroMagico)) {
				linha = PGMreader.le_linha(arquivo);
				while (linha.startsWith("#")) {
					linha = PGMreader.le_linha(arquivo);
				}
			    Scanner in = new Scanner(linha); 
			    if(in.hasNext() && in.hasNextInt())
			    	width = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
			    if(in.hasNext() && in.hasNextInt())
			    	height = in.nextInt();
			    else
			    	System.out.println("Arquivo corrompido");
				linha = PGMreader.le_linha(arquivo);
				in.close();
				in = new Scanner(linha);
				maxVal = in.nextInt();
				in.close();
				
				
				imagem_pgm = new BufferedImage(width, height, BufferedImage.TYPE_3BYTE_BGR);
				//imagem_pgm_negativo = new BufferedImage(width, height, BufferedImage.TYPE_BYTE_GRAY);
				byte [] pixels = ((DataBufferByte) imagem_pgm.getRaster().getDataBuffer()).getData();
				//byte [] pixels_negativo = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
				
				char[] camada = new char[height*width];
				int j = 0;
					if (escolha_do_filtro > 3 && escolha_do_filtro < 8){
						for (int i = 0; i < (height*width); i++){
							bb =(byte) arquivo.read();
							camada[i] =(char) bb;
							pixels[i] = bb;
						}
						
					}
					if ((escolha_do_filtro > 0 && escolha_do_filtro < 4) || (escolha_do_filtro == -8)){
					for (int i = 0; i < (height*width*3); i++){
						bb =(byte) arquivo.read();						
						if (j == 3)
							j = 0;
						if (j == 0)
							pixels[i+2] = bb;
						if (j == 1)
							pixels[i] = bb;
						if (j == 2){
							pixels[i-2] = bb;
						}
						j++;
						}
					}
					//count = 0;
			//	while(count < (height*width)) {
			//		bb = (byte) arquivo.read();
			//		pixels[count] = bb;
			//		//pixels_negativo[count] = (byte) (maxVal - bb);
			//		count++;
			//	}
			//}
			//else {
			//	System.out.println("Arquivo inválido");
			//}
			
					
					
					//JFrame frame = new JFrame();
					//frame.getContentPane().setLayout(new FlowLayout());
					//frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm)));
					//frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo)));
					//frame.pack();
					//frame.setVisible(true);
					
			LerImagem imagem = new LerImagem();
			imagem.setAltura(height);
			imagem.setLargura(width);
			imagem.setCor(maxVal);
			imagem.setCamada(camada, width, height);
			decifra decifra = new decifra();
			decifra.setEscolha_do_filtro(escolha_do_filtro);
			decifra.setPosicao(posicao);
			ImagemPGM pgm = new ImagemPGM();
			pgm.FiltroBytes(decifra.getPosicao(), imagem.getAltura(), imagem.getLargura(), decifra.getEscolha_do_filtro(), imagem.getCamada());
			pgm.FiltroNegativo(decifra.getEscolha_do_filtro(), imagem.getLargura(), imagem.getAltura(), imagem.getCor(), pixels);
			pgm.FiltroBlur(decifra.getEscolha_do_filtro(), imagem.getLargura(), imagem.getAltura(), pixels);
			pgm.FiltroSharpen(decifra.getEscolha_do_filtro(), imagem.getLargura(), imagem.getAltura(), pixels);
			ImagemPPM ppm = new ImagemPPM();
			ppm.FiltroRGB(imagem.getAltura(), imagem.getLargura(), decifra.getEscolha_do_filtro(), pixels);
			ppm.FiltroNegativo(decifra.getEscolha_do_filtro(), imagem.getLargura(), imagem.getAltura(), imagem.getCor(), pixels);
			
			//System.out.println("Height=" + height);
			//System.out.println("Width=" + width);
			//System.out.println("Total de Pixels = " + (width * height));
			//System.out.println("Total de Pixels lidos = " + count);
			arquivo.close();
		}
		}
		catch(Throwable t) {
			t.printStackTrace(System.err) ;
			return ;
		}
	}
}