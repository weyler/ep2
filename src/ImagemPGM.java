import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;


public class ImagemPGM {
	private int lsb;
	private char letra;
	
	public ImagemPGM(){
	}
	
	public void FiltroBytes(int posicao, int altura, int largura, int escolha_do_filtro, char[] camada){
		int k = 0;
		int x = 0;
		int tamanho = altura*largura;
		String letraString = "";
		List lista = new ArrayList();
		if(escolha_do_filtro == 4){
			for (int i = 0; i < tamanho; i++){
				letra = 0;
			 	while(i >= posicao){
					for(int j = 0; j < 8; j++) {
						if (j == 0){
							lsb = camada[i] & 0x01;
							letra =(char)lsb;
						}
						if(j > 0){
							lsb = camada[i] & 0x01;
							letra = (char)(letra << 1);
							letra = (char)(letra | lsb);
						}
						i++;
					}
					
					//System.out.print(letra);
					if (k > 0){
						if (letra == '#') {
							x = i;
							i = 0;
							break;
						}
					}
					lista.add(letra);
					k++;
			 	}
			 	if (k > 0){
			 	if(letra == '#'){
			 		break;
			 	}
			 	}
			}
			letraString = lista.toString();
			String nova = letraString.replace(", " , "");
			nova = nova.replace("]", "");
			nova = nova.replace("[", "");
			//System.out.println(nova);
			
			JFrame frame2 = new JFrame("Mensagem Escondida");
            frame2.setSize(500,500);

            JTextArea header = new JTextArea();      
            header.setEditable(false);
            JScrollPane areaScrollPane = new JScrollPane(header);
            header.setLineWrap(true);
            header.setWrapStyleWord(true);
            
            areaScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
            areaScrollPane.setPreferredSize(new Dimension(250, 250));

            frame2.addWindowListener(new WindowAdapter() {
               public void windowClosing(WindowEvent windowEvent){
      	        System.exit(0);
               }        
            });    
            JPanel control = new JPanel();

            frame2.add(header);
            frame2.setVisible(true);
            
            header.setText(nova);
		}
	}
	public void FiltroNegativo(int escolha_do_filtro, int largura, int altura, int cor, byte[] pixels){
		if (escolha_do_filtro == 5){
			BufferedImage imagem_pgm_negativo = new BufferedImage(largura, altura, BufferedImage.TYPE_BYTE_GRAY);
			byte [] pixels_negativo = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
			int count = 0;
			while(count < (altura*largura)) {
				pixels_negativo[count] = (byte) (cor - pixels[count]);
				count++;
			}
			JFrame frame = new JFrame();
			frame.getContentPane().setLayout(new FlowLayout());
			frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo)));
			frame.pack();
			frame.setVisible(true);
		}
		
	}
	
	public void FiltroBlur(int escolha_do_filtro, int largura, int altura, byte[] pixels){
		BufferedImage imagem_pgm_blur = new BufferedImage(largura, altura, BufferedImage.TYPE_BYTE_GRAY);
		byte [] pixels_blur= ((DataBufferByte) imagem_pgm_blur.getRaster().getDataBuffer()).getData();
		if (escolha_do_filtro == 6){
		int filter[] = {1,1,1,1,1,1,1,1,1};
		for(int i = 1; i < (largura-1); i++){
			for(int j = 1; j < (altura-1); j++){
				int value = 0;
				for(int x = -1; x <= 1; x++){
					for(int y= -1; y <=1; y++){
						value += filter[(x+1) + (3*(y+1))]* pixels[(i+x) + ((y+j)*largura)];
					}
				}
				value /= 9;
				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;
				pixels_blur[i + (j*largura)] = (byte) value;
			}
		}
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_blur)));
		frame.pack();
		frame.setVisible(true);
		}
	}
	
	public void FiltroSharpen(int escolha_do_filtro, int largura, int altura, byte[] pixels){
		BufferedImage imagem_pgm_sharpen = new BufferedImage(largura, altura, BufferedImage.TYPE_BYTE_GRAY);
		byte [] pixels_blur= ((DataBufferByte) imagem_pgm_sharpen.getRaster().getDataBuffer()).getData();
		if (escolha_do_filtro == 7){
		int filter[] = {0,-1,0,-1,5,-1,0,1,0};
		for(int i = 1; i < (largura-1); i++){
			for(int j = 1; j < (altura-1); j++){
				int value = 0;
				for(int x = -1; x <= 1; x++){
					for(int y= -1; y <=1; y++){
						value += filter[(x+1) + (3*(y+1))]* pixels[(i+x) + ((y+j)*largura)];
					}
				}
				value /= 1;
				value = value < 0 ? 0 : value;
				value = value > 255 ? 255 : value;
				pixels_blur[i + (j*largura)] = (byte) value;
			}
		}
		JFrame frame = new JFrame();
		frame.getContentPane().setLayout(new FlowLayout());
		frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_sharpen)));
		frame.pack();
		frame.setVisible(true);
		}
	}
}
