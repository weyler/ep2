import java.awt.FlowLayout;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;


public class ImagemPPM {
	
	public ImagemPPM(){
	}
	
	public void FiltroRGB(int altura, int largura, int escolha_do_filtro, byte[] pixels){
		BufferedImage imagem_ppm = new BufferedImage(largura, altura, BufferedImage.TYPE_3BYTE_BGR);
		byte [] nova_imagem = ((DataBufferByte) imagem_ppm.getRaster().getDataBuffer()).getData();
		int tamanho = altura*largura;
		if ((escolha_do_filtro > 0 && escolha_do_filtro < 3) || (escolha_do_filtro == -8)){
			for (int i = 0; i < (tamanho-2)*3; i++) {
				if (escolha_do_filtro == -8){
					nova_imagem[i]= pixels[i];
					nova_imagem[++i] = 0;
					nova_imagem[++i] = 0;
				}
				if (escolha_do_filtro == 1){
					nova_imagem[i] = 0;
					nova_imagem[++i] = pixels[i];
					nova_imagem[++i] = 0;
				}
				if (escolha_do_filtro == 2){
					nova_imagem[i] = 0;
					nova_imagem[++i] = 0;
					nova_imagem[++i] = pixels[i];
				}
			}
			JFrame frame = new JFrame();
			frame.getContentPane().setLayout(new FlowLayout());
			frame.getContentPane().add(new JLabel(new ImageIcon(imagem_ppm)));
			frame.pack();
			frame.setVisible(true);
		}
	}
	public void FiltroNegativo(int escolha_do_filtro, int largura, int altura, int cor, byte[] pixels){
		if (escolha_do_filtro == 3){
			BufferedImage imagem_pgm_negativo = new BufferedImage(largura, altura, BufferedImage.TYPE_3BYTE_BGR);
			byte [] pixels_negativo = ((DataBufferByte) imagem_pgm_negativo.getRaster().getDataBuffer()).getData();
			int count = 0;
			while(count < ((altura*largura)-2)*3) {
				pixels_negativo[count] = (byte) (cor - pixels[count]);
				count++;
			}
			JFrame frame = new JFrame();
			frame.getContentPane().setLayout(new FlowLayout());
			frame.getContentPane().add(new JLabel(new ImageIcon(imagem_pgm_negativo)));
			frame.pack();
			frame.setVisible(true);
		}
	}
}