
public class LerImagem {
	private int altura;
	private int largura;
	private int cor;
	private char[] camada;
	private byte[] pixels;

	public char[] getCamada() {
		return camada;
	}

	public void setCamada(char[] camada, int width, int height) {
		this.camada = new char[width*height];
		for (int i = 0; i < (width*height); i++){
		this.camada[i] = camada[i];
		}
	}

	public LerImagem(){
	}
	
	public int getAltura() {
		return altura;
	}
	public void setAltura(int altura) {
		this.altura = altura;
	}
	public int getLargura() {
		return largura;
	}
	public void setLargura(int largura) {
		this.largura = largura;
	}
	public int getCor() {
		return cor;
	}
	public void setCor(int cor) {
		this.cor = cor;
	}
}
